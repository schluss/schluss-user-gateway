# Schluss User Gateway

A relay service & online presence for people using the Schluss app whilst protecting their privacy.


### Requirements
- NodeJS

#### Configuration:   
Rename .env.example to .env and modify the configuration parameters.

# Running via Docker
Docker allows you to run the server without having Node.js installed:

###  Docker build with tag : schluss-user-gateway 
For more options please see https://docs.docker.com/engine/reference/commandline/build/

`docker build -t schluss-user-gateway . `

###  Docker run 

-i = --interactive ( Keep STDIN open even if not attached) \
-t = --tty ( Allocate a pseudo-TTY ) \
-d = --detach ( run container in the background) \

For more options please see https://docs.docker.com/engine/reference/commandline/run/ 

`docker run -itd -p <Host Port>:3000 schluss-user-gateway `

###  Docker list active containers 
`docker container ls`

### Docker stop active containers ( Container ID Retrieved from container list ) 
`docker stop <Container ID>`

For more Docker Options see https://docs.docker.com/reference/
