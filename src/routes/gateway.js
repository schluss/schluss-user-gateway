"use strict";

const router = require('express').Router();
const controller = require('../controllers/GatewayController.js');

// Acount
router.post('/register', controller.postRegister);
router.post('/unregister', controller.jsonParser, controller.postUnregister);

// Methods for setting up a connection between App users
router.post('/connections', controller.jsonParser, controller.postConnection);
router.get('/connections/:objectId', controller.getConnection);

// Messages
router.post('/messages', controller.jsonParser, controller.postMessage);

// Logs
router.get('/logs/:objectId/:readToken', controller.getLogs);

// Test
//router.get('/test', controller.doTest);
//router.post('/register', controller.jsonParser, controller.postRegister);
//router.post('/sendmessage', controller.jsonParser, controller.sendMessage);

module.exports = router;