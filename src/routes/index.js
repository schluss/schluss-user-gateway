"use strict";

const router = require('express').Router();

// define sub-routes
router.use('/', require('./gateway'));
router.use('/links', require('./links'));
router.use('/triggers', require('./triggers'));

router.get('/', (req, res) =>{
	
	return res.message(200, 'Schluss Gateway');
	
});

router.get('/favicon.ico', (req, res) => {res.status(204).end()});

// default route: when no route matches
router.get('*', function (req, res) {
	return res.message(400, 'Invalid route');
});

module.exports = router;