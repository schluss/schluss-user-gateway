"use strict";

const router = require('express').Router();
const controller = require('../controllers/TriggersController.js');

router.post('/', controller.jsonParser, controller.postTrigger);
router.delete('/', controller.deleteTrigger);

router.post('/release', controller.jsonParser, controller.postRelease);

module.exports = router;