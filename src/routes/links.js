"use strict";

const router = require('express').Router();
const controller = require('../controllers/LinksController.js');


// Links
router.post('/', controller.jsonParser, controller.postLink); // Authorization header: [key]/[secret]
router.put('/:objectId', controller.jsonParser, controller.putLink); // Authorization header: [linkKey]/[ownerKey]
router.head('/:objectId', controller.jsonParser, controller.headLink); // Authorization header: [linkKey]/[ownerKey]
router.delete('/:objectId', controller.jsonParser, controller.deleteLink); // Authorization header: [key]/[secret]
// router.get('/:objectId', controller.jsonParser, controller.getLink); // Authorization header: [key]/[secret]


// Shares
router.post('/:objectId/shares', controller.jsonParser, controller.postShare); // Authorization header: [linkKey]/[ownerKey]
router.get('/:objectId/shares/:shareId', controller.jsonParser, controller.getShare); // Authorization header: [linkKey]
router.head('/:objectId/shares/:shareId', controller.jsonParser, controller.getShare); // Authorization header: [linkKey]
router.delete('/:objectId/shares/:shareId', controller.jsonParser, controller.deleteShare); // Authorization header: [linkKey]/[ownerKey]
router.delete('/:objectId/shares', controller.jsonParser, controller.deleteShares); // Authorization header: [key]/[secret]


// special: delete all shares:
// router.delete('/:objectId/shares', controller.jsonParser, controller.deleteShare); // Authorization header: [linkKey]/[ownerKey]


/*

router.head('/links/:objectId/:readToken', controller.headLink);
router.get('/links/:objectId/:readToken', controller.jsonParser, controller.getLink);
router.delete('/links/:objectId/:readToken', controller.deleteLink);

// Shares 

router.delete('/shares', controller.jsonParser, controller.deleteShare);
*/

module.exports = router;