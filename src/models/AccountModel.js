"use strict";

const crypto = require('../utils/crypto.js');
const adapters = require('../utils/adapters.js');
const safeCompare = require('safe-compare');

// Load storage provider
const storage = adapters.get(process.env.STORAGE_PROVIDER);

const AccountModel = {
	
	exists : async (userId) => {
		return await storage.exists(userId);
	},
	
	getAccount : async (userId) => {
		
		// check key 
		if (!await storage.exists(userId)){
			return false;
		}
		
		// get the user Object
		const user = await storage.get(userId);
		const userObject = JSON.parse(user);
		
		return userObject;
	},
	
	createAccount :  async ()  => {
				
		// generate new secret, this represents the user, later this should be a different registration process
		const secret = crypto.hash(32); 
		
		// hash the secret, to be able to compare when authorizing user
		const secretHash = crypto.hashSha256(secret);
				
		// store user as JSON object
		const userObject = {
			created : Date.now(),
			userdata : {}, // future: (encrypt with secret) some user details or other specific data?
			appSessions : [],
			messagesQueue : [],
			secretHash : secretHash
			
		};
		
		const objectId = await storage.store(JSON.stringify(userObject));
		
		return { 
			key : objectId, 
			secret : secret 
		};
	},
	
	updateAccount : async (userId, userObject) => {
		
		// check key 
		if (!await storage.exists(userId)){
			return false;
		}
		
		return await storage.update(userId, JSON.stringify(userObject));
	},
	
	addMessageToQueue : async (userId, message) => {
		
		// check key 
		if (!await storage.exists(userId)){
			return false;
		}
		
		let messages = [];
		messages.push(message);		
		
		return await storage.patch(userId, {messagesQueue : messages});
	},
	
	validateLogin :  async (key, secret)  => {

		if (!key || !secret) {
			return false;
		}
		
		// get the user Object
		const userObject = await AccountModel.getAccount(key);

		if (!userObject){
			return false;
		}
		
		// reconstruct and compare the secretHash
		const secretHash = crypto.hashSha256(secret);
		
		if (!safeCompare(userObject.secretHash, secretHash)) {
			return false;
		}
		
		return true;
	},
	
	createSession :  async (key)  => {
		
		// generate session token - this will act as an access token to be used by the user gateway
		const token = crypto.hash(32);
		
		// generate sessionId - this will match the session to the user (to be stored at the user Object and at the session Object)
		const sessionId = crypto.hash(32);

		// hash the token, to be able to compare when authorizing session
		const tokenHash = crypto.hashSha256(token);

		// construct the session Object
		const sessionDataEnc = crypto.encrypt(JSON.stringify({
			key : key,
			sessionId : sessionId
		}), token);
		
		const session = {
			created : Date.now(),
			session : sessionDataEnc,
			tokenHash : tokenHash
		};
		
		const objectId = await storage.store(JSON.stringify(session));
					
		// store sessionId at user Object so we can keep track of all sessions connected to this user
		let sess = [];
		sess.push(objectId);
		storage.patch(key, {appSessions : sess});
		
		return objectId + '/' + token;
	},
	
	validateSession :  async (credential)  => {
				
		if (!credential || !credential.includes('/')) {
			return false;
		}
		
		// split up credential
		const [ sessionObjectId , token ] = credential.split('/');
		
		// check if session object exists
		if (!await storage.exists(sessionObjectId)) {
			return false;	
		}
		
		const session = JSON.parse(await storage.get(sessionObjectId));
		
		// compare stored token with recieved token
		const tokenHash = crypto.hashSha256(token);
		
		if (!safeCompare(session.tokenHash, tokenHash)) {
			return false;	
		}
		
		return true;
	},
	
	invalidateSession :  async (key, credential)  => {
				
		// split up credential
		const sessionObjectId = credential.split('/')[0];
		
		// delete the session Object
		storage.delete(sessionObjectId);
		
		// get the user Object
		const userObject = await AccountModel.getAccount(key);
		const removeIndex = userObject.appSessions.indexOf(sessionObjectId);
		userObject.appSessions.splice(removeIndex, 1);
		
		return await storage.update(key, JSON.stringify(userObject));
	
	},
	
	invalidateAllSessions : async (key)  => {

		// get the user object
		const userObject = await AccountModel.getAccount(key);
		
		// delete the session objects	
		userObject.appSessions.forEach(sessionObjectId => {
			storage.delete(sessionObjectId);
		})
		
		// clear session id's at user object
		userObject.appSessions = [];
		
		return await storage.update(key, JSON.stringify(userObject));
	},
	
	deleteAccount : async (key)  => {
		
		// first invalidate all sessions
		AccountModel.invalidateAllSessions(key).then(()=> {
			
			// delete the user object
			storage.delete(key);
		});
	},
	
};

module.exports = AccountModel;
