"use strict";

const bodyParser = require('body-parser');
const plugins = require('../utils/plugins.js');
const adapters = require('../utils/adapters.js');
const crypto = require('../utils/crypto.js');
const expiry = require('../utils/expiry.js');
const openpgp = require('openpgp');
const safeCompare = require('safe-compare');
const AccountModel = require('../models/AccountModel.js');

const {send} = require('../utils/sockets.js');

// Load storage provider
const storage = adapters.get(process.env.STORAGE_PROVIDER);

//const messages = [];
let counter = 1;

const GatewayController = {
	
	jsonParser : bodyParser.json({type: 'application/json', limit : '256kb'}),
	
	// register new user
	// returns a key + secrect (substitute for username + password)
	postRegister : async (req, res, next)  => {
		
		try {

			// create a new account
			const account = await AccountModel.createAccount();

			return res.message(201, {
				'key' : account.key,
				'secret' : account.secret
			});		
		}
		catch(err) {
			return next(err);
		}		
	},
	
	// remove the user + all data <- still a little issue over here: we do not know which data is of which user
	/*
	body:
	{
		'key' : [users' key],
		'secret' : [users' secret],
	}
	*/
	postUnregister :  async (req, res, next)  => {
		try {
			
			const key = req.body.key;
			const secret = req.body.secret;
			
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);
			
			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}

			// delete the useraccount (also invalidate all sessions in the process)
			//Todo: delete all users' objects (for now first delete all objects one by one and then call unregister)
			AccountModel.deleteAccount(key);
			
			
			return res.message(200,'Account deleted');  		
		}
		catch(err) {
			return next(err);
		}	
	},
	
	
	// Initialize a new connection request, to make a connection between two Schluss clients
	postConnection: async (req, res, next) => {
		
		try {
		
			// authorization token, consists of [key]/[secrect]
			const token = req.headers['x-access-token'] || req.headers.authorization;
			
			if (!token || !token.includes('/') || !req.body.type || !req.body.firstName || !req.body.lastName || !req.body.gatewayUrl || !req.body.clientKey || !req.body.clientPublicKey || !req.body.sessionId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let tokenArr = token.split('/');
			const key = tokenArr[0];
			const secret = tokenArr[1];
			
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);
			
			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}	

			
			// generate key for encrypting the body
			let cipher = crypto.hash(32);

			const body = req.body;
				
			const encBody = crypto.encrypt(JSON.stringify(body), cipher);

			const objectId = await storage.store(JSON.stringify(encBody));
			
			// Make sure the request auto expires in 1 hour from now
			await expiry.registerObject(objectId, '1h');
			
			// get the host (https://url.to.api)
			const host = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');

			
			return res.message(200,{
				token : cipher,
				objectId : objectId,
				location : host + '/connections/' + objectId
			});
		
		} 
		catch(e){
			return res.message(500, 'Something went wrong, details: ' + e.message);
		}
		
	},
	
	// Initialize a new connection request, to make a connection between two Schluss clients
	getConnection: async (req, res, next) => {
		
		try {
			
			// authorization token, consists of [key]/[secrect]
			const token = req.headers['x-access-token'] || req.headers.authorization;
			
			const objectId = req.params.objectId;
			
			if (!token || !objectId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401,'Access denied. Incorrect credentials');	
			}		

			// get the object as json
			const objectJson = JSON.parse(await storage.get(objectId));
								
			// decrypt content
			const connection = JSON.parse(crypto.decrypt(objectJson, token));	

			// get the host (https://url.to.api)
			const host = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');

			// delete the connection request, because it is now accessed and should not be accessed anymore
			storage.delete(objectId);

			return res.message(200,{
				connectionType : connection.connectionType ? connection.connectionType : 'default',
				firstName : connection.firstName,
				lastName : connection.lastName,
				clientKey : connection.clientKey,
				clientPublicKey : connection.clientPublicKey,
				sessionId : connection.sessionId,
				socketUrl : host
			});
			
		} 
		catch(e){
			return res.message(500, 'Something went wrong, details: ' + e.message);
		}
	},	
	
	postMessage: async (req, res, next) => {
		
		const to = req.body.to;
		const message = req.body.message;
	
		// check if object exists
		if (!await storage.exists(to)){
			return res.message(404,'Receiver not found');	
		}
	
		send(to, message);

		return res.message(200,'Ok');
	},

	getLogs : async (req, res, next) => {
		
		try {
			
			// get token
			const token = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			const cipher = req.params.readToken;
		
			if (!token || !cipher) {
				return res.message(401, 'Access denied', 'Incorrect credentials');
			}
			
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}
			
			// get the content as json object
			const objectJson = JSON.parse(await storage.get(objectId));
		
			// time-safe compare token with stored writeTokenHash
			const tokenHash = crypto.hashSha256(token);

			if (!safeCompare(objectJson.tokenHash, tokenHash)) {
				return res.message(401, 'Access denied', 'Incorrect credentials');
			}

			return res.message(200,{entries : objectJson.logs});	
		}
		catch(err){
			return next(err);
		}
	}
	
};


module.exports = GatewayController;
