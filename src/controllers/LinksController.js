"use strict";

const bodyParser = require('body-parser');
const plugins = require('../utils/plugins.js');
const adapters = require('../utils/adapters.js');
const crypto = require('../utils/crypto.js');
const expiry = require('../utils/expiry.js');
const openpgp = require('openpgp');
const safeCompare = require('safe-compare');
const AccountModel = require('../models/AccountModel.js');
const { v4: uuidv4 } = require('uuid');

const {send} = require('../utils/sockets.js');

// Load storage provider
const storage = adapters.get(process.env.STORAGE_PROVIDER);

//const messages = [];
let counter = 1;

const LinksController = {
	
	jsonParser : bodyParser.json({type: 'application/json', limit : '256kb'}),
	
	/* 
	 * Return information about a link
	 * header: Authorization [linkKey]/[ownerKey]
	**/
	headLink : async (req, res, next) => {
		try {
			
			res.set('Content-Type', 'text/plain');
			
			// authorization token, consists of [linkKey]/[ownerKey]
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			const linkId = req.params.objectId;
			
			if (!authToken || !authToken.includes('/')){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [linkKey, ownerKey] = authToken.split('/');
			
			// check if link exists
			if (!await storage.exists(linkId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}	
			
			// get link
			const objJson = JSON.parse(await storage.get(linkId));
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);
			
			// Now determine of we are dealing with the owner by comparing the ownerKey
			const ownerKeyHash = crypto.hashSha256(ownerKey);

			if (!safeCompare(linkObjJson.ownerKeyHash, ownerKeyHash)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// set the headers			
			res.set('AttributeName', linkObjJson.name);
			res.set('AttributeLabel', linkObjJson.label);
			res.set('AttributeGroup', linkObjJson.group);			
			res.set('AttributeCategory', linkObjJson.category);			
			res.set('AttributeLogoUrl', linkObjJson.logoUrl);			
			res.set('AttributeProvider', linkObjJson.providerName);			
			res.set('AttributeVersion', linkObjJson.version);			
			res.set('AttributeMediaType', linkObjJson.mediaType);
			res.set('AttributeType', linkObjJson.type);
			
			// append log message to logs
			const logMessage = { 
				time : Date.now(), 
				message : 'head',
				actor :  ''
			 };
			
		
			// update access log
			storage.patch(linkId, {logs : [logMessage]});
			
			res.status(200);
			return res.send();	

		}
		catch(err){
			
			return next(err);
		}
	},	

	
	/* 
	 * Ceate a new link
	 * Registers a new link to an object stored at an external storage provider
	 * Header: Authorization [key/secret]
	 * body (JSON):
	 * {
	 *  "name":"firstname",
	 *  "group":"name",
	 *  "category":"identity",
	 *  "type":"string",
	 *  "logoUrl":"https://url.to/logo.svg",
	 *  "providerName":"BRP",
	 *  "version":"1",
	 *  "mediaType":"",
	 *  "provider":"schluss/solid/dropbox",
	 *  "location":"https://url.to/stored-object-id",
	 *  "authorizationHeaders": {"authorization" : "storageprovider-auth-token"}	// or whatever headers are needed in for the given provider		 
	 * }
	**/
	postLink : async (req, res, next) => {
		
		// todo: when creating the link: add a check which checks if the attribute value is accessible using the provided location,provider and authorization
		
		try {
			
			// authorization token, consists of [key]/[secret]
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			
			if (!authToken || !authToken.includes('/')){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [key, secret] = authToken.split('/');
			
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);
			
			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}	
					

			// generate owner key which proves ownership (write access)
			const ownerKey = crypto.hash(32);
			
			// generate cipher
			const linkKey = crypto.hash(32);
						
			// generate hash from ownerKey so we can later on compare hashes to know if the user 'owns' the link
			const ownerKeyHash = crypto.hashSha256(ownerKey);
			
			// construct content to be stored in encrypted form
			const linkObj = {
				name : req.body.name,
				label : req.body.label,
				group : req.body.group,
				category : req.body.category,
				type : req.body.type,
				logoUrl : req.body.logoUrl,
				providerName : req.body.providerName,
				version : req.body.version,
				mediaType : req.body.mediaType,
				storageLocation : req.body.storageLocation,
				storageProvider : req.body.storageProvider,
				storageAuthorization : req.body.storageAuthorization,
				ownerKeyHash : ownerKeyHash, 	// to be able to determine the owner of this link
			};
			
			// encrypt link object with linkKey
			const encBody = crypto.encrypt(JSON.stringify(linkObj), linkKey);		
			
			// construct the link object to be stored
			const body = {
				encBody : encBody,
				shares : {},					// for shares to come 
				logs : [{ time : Date.now(), message : 'created', actor : ''}]	// audit logging
			};
			
			const objectId = await storage.store(JSON.stringify(body));

			// todo: store a reference -somewhere & encrypted- at the user account to this object so when the user deletes his account, all shares are deleted too

			// when link is set to expire, register it at the expiry util
			if ('expiry' in req.body){
				expire.registerObject(objectId, req.body.expiry);
			}			
			
			// construct the host part of the url (https://url.to.api)
			const host = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');
			
			return res.message(200,{
				objectId : objectId,
				linkKey : linkKey,
				ownerKey : ownerKey,
				location : host + '/links/' + objectId
			});
		}
		catch(err){
			
			return next(err);
		}
		
	},	
	
	/* 
	 * Update an existing link
	 * Registers a new link to an object stored at an external storage provider
	 * Header: Authorization [key/secret]
	 * body (JSON):
	 * {
	 *  "name":"firstname",
	 *  "group":"name",
	 *  "category":"identity",
	 *  "type":"string",
	 *  "logoUrl":"https://url.to/logo.svg",
	 *  "providerName":"BRP",
	 *  "version":"2",
	 *  "mediaType":"",
	 *  "provider":"schluss/solid/dropbox",
	 *  "location":"https://url.to/stored-object-id",
	 *  "authorizationHeaders": {"authorization" : "storageprovider-auth-token"}	// or whatever headers are needed in for the given provider		 
	 * }
	 * header: Authorization [linkKey]/[ownerKey]
	**/
	putLink : async (req, res, next) => {
		try {
			
			// authorization token, consists of [linkKey]/[ownerKey]
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			
			if (!authToken || !authToken.includes('/')){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [linkKey, ownerKey] = authToken.split('/');
			
			// check if link exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}	
			
			// get link
			const objJson = JSON.parse(await storage.get(objectId));
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);
			
			// Now determine of we are dealing with the owner by comparing the ownerKey
			const ownerKeyHash = crypto.hashSha256(ownerKey);

			if (!safeCompare(linkObjJson.ownerKeyHash, ownerKeyHash)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}

			// update the link object content
			const linkObjBody = {
				name : req.body.name,
				label : req.body.label,
				group : req.body.group,
				category : req.body.category,
				type : req.body.type,
				logoUrl : req.body.logoUrl,
				providerName : req.body.providerName,
				version : req.body.version,
				mediaType : req.body.mediaType,
				storageLocation : req.body.storageLocation,
				storageProvider : req.body.storageProvider,
				storageAuthorization : req.body.storageAuthorization,
				ownerKeyHash : ownerKeyHash, 	// to be able to determine the owner of this link
			};
						
			// encrypt link object with linkKey
			const encBody = crypto.encrypt(JSON.stringify(linkObjBody), linkKey);		
			
			// reconstruct the link object to be stored
			objJson.encBody = encBody;
			
			// append log message to logs
			const logMessage = { 
				time : Date.now(), 
				message : 'update link version to ' + linkObjBody.version,
				actor : ''
			};		
				
			objJson.logs.push(logMessage);
			
			await storage.update(objectId, JSON.stringify(objJson));

			// todo: when expiry is set, remove the current expiry and update with the new expiry
			
			// construct the host part of the url (https://url.to.api)
			const host = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');
			
			return res.message(200,{
				objectId : objectId,
				linkKey : linkKey,
				ownerKey : ownerKey,
				location : host + '/links/' + objectId
			});
		}
		catch(err){
			
			return next(err);
		}
	},
		
	
	/* 
	 * Delete a link including all shares
	 * header: Authorization [key]/[secret]
	**/
	deleteLink : async (req, res, next) => {
		try {

			const objectId = req.params.objectId;	
			
			// authorization token, consists of [key]/[secret]
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			
			if (!authToken || !authToken.includes('/') || !objectId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [key, secret] = authToken.split('/');
			
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);

			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}	
			
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}	

			// delete the object
			await storage.delete(objectId);
			
			return res.message(200,'Link deleted');
		}
		catch(err){
			
			return next(err);
		}
		
	},	
		
	/* 
	 * Ceate a new share
	 * Add a share to an existing link
	 * Header: Authorization [linkKey/ownerKey]
	 * body (JSON):
	 * {
	 *  "publicKeyHash":"hash of the public key",	 
	 * }
	 */
	postShare : async (req, res, next) => {
		try {
			
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			
			if (!authToken || !authToken.includes('/') || !objectId || !req.body.publicKeyHash){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [linkKey, ownerKey] = authToken.split('/');
			
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}
			
			// get the content as json object
			const objJson = JSON.parse(await storage.get(objectId));
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);
			
			// Now determine of we are dealing with the owner by comparing the ownerKey
			const ownerKeyHash = crypto.hashSha256(ownerKey);

			if (!safeCompare(linkObjJson.ownerKeyHash, ownerKeyHash)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// create shareId
			const shareId = uuidv4();

			// add the share
			objJson.shares[shareId] = req.body.publicKeyHash;
			
			// append log message to logs
			const logMessage = { 
				time : Date.now(), 
				message : 'add share ' + shareId,
				actor : ''
			};		
				
			objJson.logs.push(logMessage);
			
			// update the object
			await storage.update(objectId, JSON.stringify(objJson));
			
			// get the host (https://url.to.api)
			const host = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');

			return res.message(200,{
				objectId : objectId,
				shareId : shareId,
				location : host + '/links/' + objectId + '/shares/' + shareId
			});
		}
			
		catch(err){
			
			return next(err);
		}
		
	},
	
	
	headShare : async (req, res, next) => {
		
		res.set('Content-Type', 'text/plain');
		
		try {
			
			const linkKey = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			const shareId = req.params.shareId;
			
			if (!linkKey || !objectId || !shareId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
						
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}
			
			// get the content as json object
			const objJson = JSON.parse(await storage.get(objectId));
			
			// check if requested share for this link object exists
			if (!(shareId in objJson.shares)){
				return res.message(401, 'Access denied', 'Incorrect credentials');
			}
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);
			
			
			// TODO: maybe add a check here to know for sure the attribute value actually (still)
			// exists at the location, if not we'd might want to return an access denied?
			// what if: external storage is temporary down?
			
	
			// set the headers			
			res.set('AttributeName', linkObjJson.name);
			res.set('AttributeLabel', linkObjJson.label);
			res.set('AttributeGroup', linkObjJson.group);			
			res.set('AttributeCategory', linkObjJson.category);			
			res.set('AttributeLogoUrl', linkObjJson.logoUrl);			
			res.set('AttributeProvider', linkObjJson.providerName);			
			res.set('AttributeVersion', linkObjJson.version);			
			res.set('AttributeMediaType', linkObjJson.mediaType);
			res.set('AttributeType', linkObjJson.type);		
			
			const logMessage = { 
				time : Date.now(), 
				message : 'head',
				actor :  shareId	// todo: publicKey hash of the requester here? -> in the future we need to be able to easily get the connected user name when displaying the logs inside the app
			 };
			
			// update access log
			storage.patch(objectId, {logs : [logMessage]});
			
			res.status(200);
			return res.send();			

		}			
		catch(err){
			res.status(500);
			res.set('Error', err);
			return res.send();

		}

		
	},	
	
	
	/* 
	 * Get share
	 * Get data from a shared link
	 * Header: Authorization [linkKey]
	 */
	getShare : async (req, res, next) => {
		try {	
		
			const linkKey = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			const shareId = req.params.shareId;
			
			if (!linkKey || !objectId || !shareId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
						
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}
			
			// get the content as json object
			const objJson = JSON.parse(await storage.get(objectId));
			
			// check if requested share for this link object exists
			if (!(shareId in objJson.shares)){
				return res.message(401, 'Access denied', 'Incorrect credentials');
			}
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);
			
			// check if plugin exists
			if (!plugins.exists(linkObjJson.storageProvider)){
				return res.message(400,'Invalid request');	
			}			
	
			// get the content of the object stored at the external storage
			const responseData = await plugins.get(linkObjJson.storageProvider).getObject(res, linkObjJson.storageLocation, linkObjJson.storageAuthorization);

			
			// set the headers			
			res.set('Content-Type', 'text/plain');	//Todo: forward content-type from plugin
			
			res.set('AttributeName', linkObjJson.name);
			res.set('AttributeLabel', linkObjJson.label);
			res.set('AttributeGroup', linkObjJson.group);			
			res.set('AttributeCategory', linkObjJson.category);			
			res.set('AttributeLogoUrl', linkObjJson.logoUrl);			
			res.set('AttributeProvider', linkObjJson.providerName);			
			res.set('AttributeVersion', linkObjJson.version);			
			res.set('AttributeMediaType', linkObjJson.mediaType);	
			res.set('AttributeType', linkObjJson.type);	
			
			const logMessage = { 
				time : Date.now(), 
				message : 'access',
				actor :  shareId	// todo: publicKey hash of the requester here? -> in the future we need to be able to easily get the connected user name when displaying the logs inside the app
			 };
			
			// update access log
			storage.patch(objectId, {logs : [logMessage]});
			
			res.status(200);
			
			return res.send(responseData);			

		}			
		catch(err){
			
			return next(err);
		}
		
	},
	
	/* 
	 * Delete a share
	 * header: Authorization [linkKey]/[ownerKey]
	**/
	deleteShare : async (req, res, next) => {
		try {	
		
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			const shareId = req.params.shareId;
			
			if (!authToken || !authToken.includes('/') || !objectId || !shareId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [linkKey, ownerKey] = authToken.split('/');
		
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}
			
			// get the content as json object
			const objJson = JSON.parse(await storage.get(objectId));
			
			
			// check if requested share for this link object exists
			if (!(shareId in objJson.shares)){
				return res.message(401, 'Access denied', 'Incorrect credentials');
			}
			
			const linkObj = crypto.decrypt(objJson.encBody, linkKey);
			
			// When HMAC check failed, or it is just the wrong linkKey
			if (linkObj === false){
				return res.message(401, 'Access denied. Incorrect credentials');		
			}
			
			const linkObjJson = JSON.parse(linkObj);			
			
			// Now determine of we are dealing with the owner by comparing the ownerKey
			const ownerKeyHash = crypto.hashSha256(ownerKey);

			if (!safeCompare(linkObjJson.ownerKeyHash, ownerKeyHash)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// We've now determined the current requester owns the share.
			
			
			// Delete share from the link
			delete objJson.shares[shareId];
				
			// append log message to logs
			const logMessage = { 
				time : Date.now(), 
				message : 'delete share ' + shareId,
				actor : ''
			};		
				
			objJson.logs.push(logMessage);
			
			// update the object
			await storage.update(objectId, JSON.stringify(objJson));
			
			return res.message(200, 'Share deleted');
		}
		catch(err){
			
			return next(err);
		}		
	},
	
	/* 
	 * Delete all shares for a link
	 * header: Authorization [key]/[secret]
	**/
	deleteShares : async (req, res, next) => {
		try {	
		
			const authToken = req.headers['x-access-token'] || req.headers.authorization;
			const objectId = req.params.objectId;
			const shareId = req.params.shareId;
			
			if (!authToken || !authToken.includes('/') || !objectId){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			let [key, secret] = authToken.split('/');
		
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);

			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}	
			
			// check if object exists
			if (!await storage.exists(objectId)){
				return res.message(401, 'Access denied', 'Incorrect credentials');	
			}		
					
			// get the content as json object
			const objJson = JSON.parse(await storage.get(objectId));
			
			console.log(objJson);
						
			// Delete all shares from the link
			objJson.shares = {};
				
			// append log message to logs
			const logMessage = { 
				time : Date.now(), 
				message : 'delete shares',
				actor : ''
			};		
				
			objJson.logs.push(logMessage);
			
			// update the object
			await storage.update(objectId, JSON.stringify(objJson));
			
			return res.message(200, 'Shares deleted');
		}
		catch(err){
			
			return next(err);
		}		
	},	
};


module.exports = LinksController;
