"use strict";

const bodyParser = require('body-parser');
const plugins = require('../utils/plugins.js');
const adapters = require('../utils/adapters.js');
const crypto = require('../utils/crypto.js');
const request = require('../utils/request.js');
const AccountModel = require('../models/AccountModel.js');
const {send} = require('../utils/sockets.js');

// Load storage provider
const storage = adapters.get(process.env.STORAGE_PROVIDER);

//const messages = [];
let counter = 1;

const TriggersController = {
	
	jsonParser : bodyParser.json({type: 'application/json', limit : '256kb'}),
	
	// Register new trigger
	// header: Authorization [key/secret]
	// body:
	/* 
	{
		trigger : {
			url : '[url]', 
			method : '[GET/POST]'
			header : '[header]'
			expect : '[response]', 
		},
		from : '[clientId]', 
		to: '[clientId]', 
		shares : [gatewayLinkIdx,y,z] 
	}		
	*/
	postTrigger : async (req, res, next)  => {
		
		try {

			// authorization token, consists of [key]/[secrect]
			const token = req.headers['x-access-token'] || req.headers.authorization;
			
			if (!token || !token.includes('/') || !req.body.trigger || !req.body.from || !req.body.to || !req.body.shares){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			if (!req.body.trigger.url.includes('https')){
				return res.message(400, 'Trigger url needs to be HTTPS');
			}
			
			if (!req.body.trigger.method.includes('POST') &&  !req.body.trigger.method.includes('GET')){
				return res.message(400, 'Trigger method needs to be POST or GET');
			}
			
			const [key , secret] = token.split('/');
			
			// validate credentials
			const user = await AccountModel.validateLogin(key, secret);
			
			if (!user){
				return res.message(401, 'Access denied. Incorrect credentials');
			}


			// generate key for encrypting the body
			let cipher = crypto.hash(32);
			
			const body = req.body;
				
			const encBody = crypto.encrypt(JSON.stringify(body), cipher);

			const objectId = await storage.store(JSON.stringify(encBody));
			
			return res.message(201, {
				'trigger' : Buffer.from(objectId + '/' + cipher, 'utf8').toString('base64')
			});		
		}
		catch(err) {
			return next(err);
		}		
	},
	
	// Unregister an existing trigger
	// Header: Authorization [trigger]
	deleteTrigger :  async (req, res, next)  => {
		try {
			
			// parse the authorization header
			const header = parseAuthHeader(req.headers['x-access-token'] || req.headers.authorization);
	
			if (!header){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			
			if (!await storage.exists(header.objectId)){
				return res.message(401, 'Access denied. Incorrect credentials');			
			}
			
			const triggerObjEnc = JSON.parse(await storage.get(header.objectId));
	
			// try to decrypt
			const triggerObj = crypto.decrypt(triggerObjEnc, header.cipher);			
	
			if (!triggerObj){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// delete the object
			storage.delete(header.objectId);
			
			return res.message(200,'Trigger deleted');  		
		}
		catch(err) {
			return next(err);
		}	
	},
		
	// fire a trigger 'release' request
	// Header: Authorization [trigger]
	/* Body 
	{
		"name": "Jan Janssen",
		"birthdate": "1935-01-03"
	}
	*/
	postRelease: async (req, res, next) => {
		
		try {
			
			// parse the authorization header
			const header = parseAuthHeader(req.headers['x-access-token'] || req.headers.authorization);

			if (!header){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			if (!await storage.exists(header.objectId)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}

			const triggerObjEnc = JSON.parse(await storage.get(header.objectId));
			
			// try to decrypt
			const triggerObjDec = crypto.decrypt(triggerObjEnc, header.cipher);			
			
			if (!triggerObjDec){
				return res.message(401, 'Access denied. Incorrect credentials');
			}		

			const triggerObj = JSON.parse(triggerObjDec);			
			
			// run the trigger
			let result = JSON.parse(await request.post(triggerObj.trigger.url, {
				method : triggerObj.trigger.method,
				headers: triggerObj.trigger.header,
				body : req.body, // add the body to the prev stored trigger information to have a complete request
				json: true,
			}));	
			
			// check and compare the expected result
			if (!deepCompare(triggerObj.trigger.expect, result)){
				return res.message(401, 'Access denied. Incorrect credentials');
			}
			
			// create a socket message to the client
			
			// check if client exists
			if (!await storage.exists(triggerObj.to)){
				return res.message(404,'Receiver not found');	
			}
			
			// compose the message
			const message = {
				type : 'attributeShareUpdate',
				from : triggerObj.from,
				hideValues : false,
				shares : triggerObj.shares
			};
			
		
			// send the socket message with the release shares
			if (!await send(triggerObj.to, JSON.stringify(message))){
				return res.message(404,'Could not send the release message');
			}
					
			// delete the object
			storage.delete(header.objectId);

			return res.message(200,{
				status : 'Trigger released'
			});
		
		} 
		catch(e){
			return res.message(500, 'Something went wrong, details: ' + e.message);
		}
		
	},	
			
};

// Deep compare two strings / objects 
function deepCompare(arg1, arg2) {
	if (Object.prototype.toString.call(arg1) === Object.prototype.toString.call(arg2)){
		if (Object.prototype.toString.call(arg1) === '[object Object]' || Object.prototype.toString.call(arg1) === '[object Array]' ){
			if (Object.keys(arg1).length !== Object.keys(arg2).length ){
				return false;
		}
		return (Object.keys(arg1).every(function(key){
			return deepCompare(arg1[key],arg2[key]);
		}));
		}
		return (arg1===arg2);
	}
	return false;
}

// authorization token, consists of [objectId]/[cipher] which is parsed and split using this method
function parseAuthHeader(header) {
	try {
		
		if (!header){
			return false
		}			
		
		// reconstruct the cipher and objectId from header (trigger)
		// base 64 decode and split into issuer object key and issuer object id
		const tmp = Buffer.from(header, 'base64').toString('utf8');
		
		if (!tmp.includes('/')){
			return false
		}
		
		let [objectId, cipher] = tmp.split('/');
		
		return { cipher : cipher, objectId: objectId };
	}
	catch{
		return false;
	}
}

module.exports = TriggersController;
