"use strict";

const express = require('express');
const helmet = require('helmet');
const routes = require('./routes');
const logger = require('./utils/logger.js');

const app = express();
const server = require('http').createServer(app);

// Init Socket server
const {init} = require('./utils/sockets.js');

init(server);

// Init processing of expired stored data
if (process.env.STORAGE_EXPIRY == 'true' || process.env.STORAGE_EXPIRY == '1'){
	const expiryUtil = require('./utils/expiry.js');
	expiryUtil.enableAutomaticExpiry(+process.env.STORAGE_EXPIRY_INTERVAL);
}

// setup CORS
app.use((req, res, next) => {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', '*');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    
	if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

// secure against different types of attacks
app.use(helmet());

// define a generic response message
express.response.message = function (httpStatus, message, detail){
	
	this.setHeader('Content-Type', 'application/json');
	
	let msg = {
		timestamp : new Date().toString(),
		status : httpStatus
	};

	// when message is an object (like a returned resource), display that instead of 'wrapping' it in a message property
	if (typeof message === 'object' && message !== null){
		msg = Object.assign(msg, message);
	}
	else {
		msg.message = message;
	}		
	
	// if detail information is available
	if (detail){
		msg.detail = detail;
	}
	
	return this.status(httpStatus).json(msg);
}

// log all requests
app.use((req, res, next) => {
	logger.log('['+ req.method + '] ' + req.url);
	return next();
});	

// connect routes
app.use('/', routes);

// log errors
app.use(logger.expressLog);
	
// handle errors
app.use(function (err, req, res, next) {
	
	if (res.headersSent) {
		return next(err);
	}
  
	return res.message(500, err.message);
});

// run the app
server.listen(process.env.PORT, () => { 
	logger.log('Schluss User Gateway listening at http://localhost:' + process.env.PORT, 'info', true);
});
	