"use strict";

const winston = require('winston');
const expressWinston = require('express-winston');



const logFormat = winston.format.printf(function(info) {
	
	let today = new Date();
	let date = today.toLocaleDateString() + ' ' + today.toLocaleTimeString();	
	
	return `${date} - ${info.level}: ${info.message}`;
});

const logger = winston.createLogger({
    level: "info",
    format: logFormat,
    transports: [
      new winston.transports.Console()
    ],
});
  
module.exports = { 

	// log a message to console
	// @param: optional type : info, error
	log : (msg, type, showAlways) =>{
						
		// do not log to console when in production
		if (process.env.NODE_ENV == 'production' && !showAlways)
			return;
			
		if (!type)
			type = 'info';
			
		logger.log({
		  level: type,
		  message: msg
		});
			
	},
	
	// Express logging middleware
	expressLog : expressWinston.errorLogger({
		transports: [
			// output to console
			new winston.transports.Console(),	
		
			// output to file
			new winston.transports.File({ filename: process.env.LOGPATH + 'error.log', level: 'error' }),
		],
			format: winston.format.combine(
			winston.format.colorize(),
			winston.format.json()
		)
	})

};