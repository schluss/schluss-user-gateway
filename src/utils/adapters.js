"use strict";
const logger = require('../utils/logger');

class AdapterManager {
	
    constructor() {	
		this.adapters = [];
    }
	
	get(name){
		
		// if not cached, load and do it right now
		if (!this.isLoaded(name)){
			this.adapters[name] = require(process.env.ROOT + 'adapters/' + name + '.js');
			
			logger.log('adapter loaded: ' + name);
			
		}
	
		return this.adapters[name];
	}
	
	isLoaded(name){
		return name in this.adapters;
	}
	
}

module.exports = new AdapterManager();
