"use strict";

const accountModel = require('../models/AccountModel.js');
const logger = require('./logger.js');
const io = require('socket.io')();

// keeps track of all connected clients
let connected = {};

// init the socketio server and attach it to a HTTP server, always call this first!
const init = (server) => {
	
	io.attach(server);
	
	// auth
	io.use(async(socket, next)  =>  {
		
		const token = socket.handshake.auth.token;
		const secret = socket.handshake.auth.secret;
	  
		logger.log('Authrequest from: ' + token);

		if (!await accountModel.validateLogin(token, secret)) {
			logger.log('Authorization failed: invalid credentials for ' + token);
			return next(new Error("invalid credentials"));
		}
		
		logger.log('Client authorized: ' + token);
	  
		// store token
		socket.token = token;
		next();
	});	
	
	// when a client establishes a connection
	io.on('connection', (socket) => { 

		// mark this store this user as being currently connected
		connected[socket.token] = socket.id;
		
		// client joins room from token
		socket.join(socket.token);
		
		// handle queue of missed messages
		processQueue(socket.token)
		
		// process messages sent from a client
		// in the future we can make this different kind of messages!
		socket.on('message', async (data) => {
			
			// when it is a message to another client, check if it exists first and then send the message
			if (data.to != undefined && data.message != undefined && (await accountModel.exists(data.to))){
				//logger.log('Direct message from ' + socket.token + ' to ' + data.to);
				send(data.to, data.message);
			}
		});	

		// when a client disconnects 
		socket.on('disconnecting', function(){
			
			logger.log('Disconnect client ' + socket.token);
			
			// remove room/client from list
			delete connected[socket.token];
		});		
	});	
}

// checks if a client is (still) connected
const isConnected = (token) => {
	return connected[token];
}

const processQueue = async (token) => {
	let userObj = await accountModel.getAccount(token);

	if (userObj.messagesQueue && Array.isArray(userObj.messagesQueue) && userObj.messagesQueue.length > 0){
		
		// clone the array (otherwise we would be changing it while looping)
		let messages = [...userObj.messagesQueue];

		logger.log('Start processing queue for ' + token);
		
		for (var key in messages){
			
			// for each message, check if client is still connected
			if (!isConnected(token)){
				logger.log('Abort processing queue because client is not connected anymore, id: ' + token);
				return false;
			}
			
			// send message to client
			io.to(token).emit('message', messages[key]);

			// remove message from queue
			userObj.messagesQueue.splice(0, 1);
			
			// message is sent, update userObj
			// could be faster, to do this after all messages sent, but we choose reliability over speed
			if(!await accountModel.updateAccount(token, userObj)){
				logger.log('Could not update account after processing a message from messagesQueue, userId: ' + token);
				break;
			}
		}
		
		logger.log('Finished processing message queue for ' + token);
	}
}

// send a message to a client
// when client is offline, the message will be stored in the queue, to be processed when client is connected again
const send = async (token, message) => {
	
	// when client is connected
	if (isConnected(token)){
		logger.log('Send message to ' + token + ':');
		logger.log(message);
		
		io.to(token).emit('message', message);
	}
	// when client is not connected
	else {
		logger.log('Queued message for ' + token + ':');
		logger.log(message);
		
		if (!await accountModel.addMessageToQueue(token, message)){
			logger.log('Could not store message for ' + token);
			
			return false;
		}
	}
	
	return true;
}

module.exports = {
	init,
	io,
	send
}

