"use strict";

const logger = require('../utils/logger');
const glob = require('glob');

class PluginManager {
	
    constructor() {
		
		this.plugins = [];
		
		glob(process.env.ROOT + 'plugins/*.js', {cwd: process.env.ROOT},  (err, files) =>
		{
			files.forEach((file) => {
				var plugin = require(file);
				this.plugins[plugin.name] = plugin;

				logger.log('plugin loaded: ' + plugin.name);

				if (this.plugins[plugin.name].init){
					this.plugins[plugin.name].init();
					
					logger.log('plugin init: ' + plugin.name);
				}
				
				
			});

		});		
    }
	
	get(name){
		return this.plugins[name];
	}
	
	exists(name){
		return name in this.plugins;
	}
	
}

module.exports = new PluginManager();