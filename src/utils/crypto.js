"use strict";

const crypto = require('crypto');
const safeCompare = require('safe-compare');

module.exports = {
	// generate a random hash
	// note, length must be an even number
	hash : (length) => {
		
		if (length %2 == 1)
			throw 'Invalid hash length';
		
		return crypto.randomBytes(length/2).toString('hex');
	},
	
	// generate a SHA256 hash from a textstring
	hashSha256 : (text) => {
		
		const hash = crypto.createHash('sha256');

		hash.update(text);
		
		return hash.digest('hex');		
	},
	
	
	encrypt : (text, key) => {
		
		const iv = crypto.randomBytes(16);
		const ivStr = iv.toString('base64');
		
		const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
		const encrypted = cipher.update(text);
		const encryptedBuffer = Buffer.concat([encrypted, cipher.final()]);
		const cipherStr = encryptedBuffer.toString('base64');
		
		let hmac = crypto.createHmac('sha256', key);
		hmac.update(ivStr);
		hmac.update(cipherStr);
		hmac.update(key);
		
		return { 
			iv: ivStr, 
			cipher: cipherStr,
			hmac : hmac.digest("base64")
		};
	},

	decrypt : function (text, key) {
		
		// hmac check
		let hmac = crypto.createHmac('sha256', key);
		hmac.update(text.iv);
		hmac.update(text.cipher);
		hmac.update(key);
		
		if (!safeCompare(hmac.digest('base64'), text.hmac)){
			return false;
		}
		
		const iv = Buffer.from(text.iv, 'base64');
		const encryptedText = Buffer.from(text.cipher, 'base64');
		const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
		const decrypted = decipher.update(encryptedText);
		const decryptedBuffer = Buffer.concat([decrypted, decipher.final()]);
		
		return decryptedBuffer.toString();
	}	
};