"use strict";

const expiryCalc = require('expiry-js');
const adapters = require('../utils/adapters.js');
const logger = require('../utils/logger');

// Load storage provider
const storage = adapters.get(process.env.STORAGE_PROVIDER);

const ExpiryUtil = {
	
	prefix : '_expiryindex',
	
	// Enable a background process to run the processExpiredObjects at given interval (in hours)
	enableAutomaticExpiry : async (interval = 1) => {
				
		let now = new Date();
		let delay = new Date(); 

		// startup the process 
		
		// calculate the moment the interval starts (the 1st sec of the upcoming hour)
		delay.setTime(delay.getTime() + (1*60*60*1000)); // go to the upcoming hour
		delay.setHours(delay.getHours(),0,1); // go to the first second of the upcoming hour	
		let delayTime = delay.getTime() - now.getTime();		
		
		logger.log('Autoexpiry - Init, set to interval: ' + interval + 'h, starting in: ' + Math.floor((delayTime / 1000) / 60) + ' min');
		
		setTimeout(function(){
			
			setInterval(function() {
				
				logger.log('Autoexpiry - Processing expired objects');
				ExpiryUtil.processExpiredObjects();
				
			}, (3600000 * interval)); // (total miliseconds in 1hr) * interval hours
		}, delayTime);
		
		
	},
	
	processExpiredObjects : async () => {
		
		try {
		
			// get current date data
			const currentDate = new Date();

			// subtract 1h from the date: we need to process expired items until now.
			// So if it is 11:25h, everything up to 11:00h (including 10:00h) will be removed
			currentDate.setHours(currentDate.getHours() - 1);

			const currentDayOfYear = ExpiryUtil.getDayOfYear(currentDate);
			const currentYear = currentDate.getFullYear();
			const currentHour = currentDate.getHours();
			
			// first get all years in the _expiryindex including currentYear
			const indexes = await storage.getAsList(ExpiryUtil.prefix);
			indexes.sort(); // make 100% sure it is in the right order, from old to new
			
			for (var i in indexes){
				
				const [year, day] = indexes[i].split('.'); // split 2021.123 in year and day
				
				// only process years and days up to this year and day
				if (year > currentYear || (year == currentYear && day > currentDayOfYear)){
					break;
				} 
				
				logger.log('Autoexpiry - process year ' + year + ', day ' + day);
				
				// open day object
				const obj = JSON.parse(await storage.get(ExpiryUtil.prefix + '/' + indexes[i]));
				
				// make a clone of obj, so we can modify the hours in it seperate from the loop
				let processed = {...obj};
				
				// process all entries, until we're up to now - 1hr
				for (var hour in obj){
				
					// when we reach the current hour, abort further processing
					if (year == currentYear && day == currentDayOfYear && hour >  currentHour){
						break;
					}
					
					for (var entry in obj[hour]){
						// delete the expired object
						storage.delete(obj[hour][entry]);
					}
						
					// remove the hour with all entries from the obj because we deleted all objects from that hour
					delete processed[hour];
				}
				
				// when all hours of current day are processed, delete the day index
				if (Object.keys(processed).length == 0){
					storage.delete(ExpiryUtil.prefix + '/' + indexes[i]);
				}
				// otherwise, there are remaining hours left and only if the object is actually changed, update the day index
				else if (JSON.stringify(obj) !== JSON.stringify(processed)){	
					await storage.update(ExpiryUtil.prefix + '/' + indexes[i], JSON.stringify(processed));
				}
			}
			
			logger.log('Autoexpiry - done');
			return true;
		
		}
		catch(err){
			logger.log(err, 'error');
		}
	},
	
	// add an object to the expiry index
	// expireIn : possible values 1Y 1M 2W 3D 5h 8m 11s 19ms, can also be combined like 2W1D5h
	// Using this format: https://www.npmjs.com/package/expiry-js#usage
	registerObject: async (objectId, expireIn) => {
		
		// calculate expiry date data
		const expireInMilliseconds = expiryCalc(expireIn).asMilliseconds();	// convert given expiry to milliseconds
		const expiryDate = new Date(new Date().getTime() + expireInMilliseconds);
		const expiryDayOfYear = ExpiryUtil.getDayOfYear(expiryDate);
		const expiryYear = expiryDate.getFullYear();
		const expiryHour = expiryDate.getHours();
				
		const currentIndex = ExpiryUtil.prefix + '/' + expiryYear + '.' + expiryDayOfYear;
		
		let indexObject = null;
		
		// check if expiry index object exists and get/ otherwise make it
		let exists = await storage.exists(currentIndex);
		
		if (exists){
			indexObject = JSON.parse(await storage.get(currentIndex));
		}
		else {
			indexObject = {};
		}
		
		// check if expiry hour is in the indexObject
		if (!(expiryHour in indexObject)){
			indexObject[expiryHour] = [];
		}
		// otherwise do a sanity check if that objectId is already there
		else if (indexObject[expiryHour].includes(objectId)){
			// it is already there, no need to continue this process 
			return true;
		}
		
		// add the objectId to the list on that expiryHour
		indexObject[expiryHour].push(objectId);
		
		if (exists){
			await storage.update(currentIndex, JSON.stringify(indexObject));
		}
		else {
			await storage.store(JSON.stringify(indexObject), currentIndex);
		}

		return true;
	},
	
	getDayOfYear : (date) => {
		return Math.floor((date - new Date(date.getFullYear(), 0, 0)) / 1000 / 60 / 60 / 24);
	},
};

module.exports = ExpiryUtil;