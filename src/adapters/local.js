"use strict";

const fs = require('fs-extra');
const { v4: uuidv4 } = require('uuid');
const nestedObjectAssign  = require('nested-object-assign');
const logger = require('../utils/logger');

// Storage adapter, providing local file storage

exports.name = 'local';

const storagePath = process.env.LOCAL_STORAGE_PATH;

exports.store = async function(content, objectId){
	
	try {
		// if no object name (id) is given, make a unique one
		if (objectId === undefined){
			objectId = uuidv4();
		}
		
		await fs.outputFile(storagePath + objectId, content);
		
		return objectId;
		
	}
	catch(err){
		logger.log(err, 'error');
		return false;
	}
}

// not sure if this one will stay, 
// maybe it's a too costly process to first check if the object exists and then return it.
// we might just immediately try to get it!
exports.exists = async function(objectId){
	return await fs.pathExists(storagePath + objectId);
}

// Return the content of a single object
exports.get = async function(objectId){
	try {
		
		if (!await exports.exists(objectId)){
			return false;
		}
		
		return await fs.readFile(storagePath + objectId, 'utf8');
	}
	catch {
		return false;
	}
}

// Return a list of names of objects that are located at the given path
exports.getAsList = async function(path){
	try {
		const objectNames = await fs.readdir(storagePath + path);
		return objectNames;
	}
	catch {
		return [];
	}
}

// Delete a stored object
exports.delete = async function(objectId){
	try {
		await fs.unlink(storagePath + objectId);
		return true;
	}
	catch {
		return false;
	}
}

// Update the content of an object by adding the changes to it. 
// Warning: it merges the new content with the existing content by overwriting and adding. Does not support removal. 
exports.patch = async function(objectId, content){
	return new Promise(async (resolve) => {
	
		if (!await exports.exists(objectId)){
			return resolve(false);
		}
		
		const object = await exports.get(objectId);
		let objectJson = JSON.parse(object);
		
		const mergedObject = nestedObjectAssign({}, objectJson, content);
		
		const result = exports.store(JSON.stringify(mergedObject), objectId);

		if (!result){
			resolve(false);
		}
		
		resolve(true);
		
	});
}

// Update the content object by overwriting it
exports.update = async function(objectId, content){
	const result = exports.store(content, objectId);
}