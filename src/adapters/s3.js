"use strict";

const nestedObjectAssign  = require('nested-object-assign');
const { v4: uuidv4 } = require('uuid');
const AWS = require('aws-sdk');
const logger = require('../utils/logger');

// Storage adapter, providing S3 compatible storage

exports.name = 's3';

const spacesEndpoint = new AWS.Endpoint(process.env.S3_ENDPOINT_URL);
const s3 = new AWS.S3({
	endpoint: spacesEndpoint,
	accessKeyId: process.env.S3_ACCESS_KEY_ID,
	secretAccessKey: process.env.S3_ACCESS_KEY_SECRET
});
const Bucket = process.env.S3_BUCKET_NAME;


exports.store = async function(content, objectId){
	return new Promise((resolve, reject) => {

		// if no object name (id) is given, make a unique one
		if (objectId === undefined){
			objectId = uuidv4();
		}

		const params = {
			Bucket: Bucket,
			Key: objectId,	
			Body : content
		};

		s3.putObject(params, function(err) {
			if (err) { 
				logger.log(err, 'error');
				return reject(err);
			}
			
			return resolve(objectId);
		});
	});
}

// not sure if this one will stay, 
// maybe it's a too costly process to first check if the object exists and then return it.
// we might just immediately try to get it!
exports.exists = async function(objectId){
	return new Promise((resolve) => {
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};

		s3.headObject(params, function(err) {
			if (err) { 
			
				if (err.code.toLowerCase() == 'notfound') {
					return resolve(false);
				}
				
				logger.log(err);
				
				return reject(err);
			}
			
			return resolve(true);
		});
	});
}

exports.get = async function(objectId){
	return new Promise(async (resolve) => {
		
		//if (!await exports.exists(objectId)){
		//	return resolve('');
		//}
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};		
		
		s3.getObject(params, function(err, data) {
			if (err){
				logger.log(err);
				return resolve('');
			}
			
			return resolve(data.Body.toString('utf8'));
		});
	});
}

// Return a list of names of objects that are located at the given path
exports.getAsList = async function(path){
	
	return new Promise(async (resolve) => {
		
		const params = {
			Bucket: Bucket,
			Prefix: path
		};		
		
		let items = await getAllKeys(params);
		
		return resolve(items);		
	});	
}

async function getAllKeys(params,  allKeys = []){
	try {
		const response = await s3.listObjectsV2(params).promise();
		// because we are not working with a 'real' filesystem, we need to remove the/path/to/filename so only 'filename' is returned. Hence the replace
		response.Contents.forEach(obj => allKeys.push(obj.Key.replace(params.Prefix + '/','')));

		if (response.NextContinuationToken) {
			params.ContinuationToken = response.NextContinuationToken;
			await getAllKeys(params, allKeys); // RECURSIVE CALL
		}
		
		return allKeys;
	} 
	catch {
		return [];
	}
}

// Delete a stored object
exports.delete = async function(objectId){
	return new Promise(async (resolve) => {
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};		
		
		s3.deleteObject(params, function(err, data) {
			if (err) {
				return resolve(false);
			}
			else {
				return resolve(true); 
			}
		});
	});
}

// Update the content of an object by adding the changes to it. 
// Warning: it merges the new content with the existing content by overwriting and adding. Does not support removal. 
exports.patch = async function(objectId, content){
	return new Promise(async (resolve) => {
	
		if (!await exports.exists(objectId)){
			return resolve(false);
		}
		
		const object = await exports.get(objectId);
		let objectJson = JSON.parse(object);
		
		const mergedObject = nestedObjectAssign({}, objectJson, content);
		
		const result = await exports.store(JSON.stringify(mergedObject), objectId);

		if (!result){
			resolve(false);
		}
		
		resolve(true);	
		/*
		const params = {
			Bucket: Bucket,
			Key: objectId,	
			Body : JSON.stringify(mergedObject)
		};

		s3.putObject(params, function(err) {
			if (err) { 
				logger.log(err);
				return reject(err);
			}
			
			return resolve(objectId);
		});*/
	
	});
}

// Update the content object by overwriting it
exports.update = async function(objectId, content){
	return exports.store(content, objectId);
}



