"use strict";

// plugin to handle communication between Gateway and Dropbox storage provider

const request = require('../utils/request.js');


exports.name = "dropbox";


exports.getObject = async function(res, body){
	
	if (!body.headers)
		return res.status(401).send("Invalid request.");
	
	// do a get request and get the result
	let result = await request.get(body.location, {
		headers : { 'Authorization' : body.authorization } ,
	});
	

	// return the result
	return result;
}