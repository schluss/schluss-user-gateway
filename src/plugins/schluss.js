"use strict";

// plugin to handle communication between Gateway and Schluss external storage provider

const request = require('../utils/request.js');

module.exports.name = 'schluss';


exports.getObject = async function(res, location, auth){

	// do a get request and get the result
	let result = await request.get(location, {
		headers : { 'Authorization' : auth.authorization } ,
	});

	// return the result
	return result;
}


module.exports.init = () => {
	// stuff to init at startup if needed
}
